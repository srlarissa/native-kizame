import styled from 'styled-components/native';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';

export const Container = styled.ImageBackground`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const LoadingIcon = styled.ActivityIndicator`
    margin-top: ${RFPercentage(10)};
    margin-bottom: ${RFPercentage(20)};

`;

export const Logo = styled.Image`
    width: ${RFValue(100)};
    height: ${RFValue(100)};
    
`;