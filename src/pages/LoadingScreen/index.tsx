import React from 'react';
import { Container, LoadingIcon, Logo } from './style';
export function LoadingScreen(){

    return (
        <Container source={require('../../imagens/bgimg.png')}>
            <Logo source={require('../../imagens/Logo.png')}/>
            <LoadingIcon size="large" color="#FFFFFF"/>
        </Container>
    );
}

