import styled from 'styled-components/native';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';

export const Container = styled.ImageBackground`
    flex:1;
    flex-direction: column;
    position: absolute;
    justify-content: center;
    align-items: center;
    width: 100%;
    height:100%;
`; 

export const Elements = styled.View`
    width: 100%;
    position:relative;
    align-items: center;
`;

export const Logo = styled.Image`
    width: ${RFPercentage(10)}px;
    height: ${RFPercentage(10)}px;

`;

export const Input = styled.TextInput`
    background-color: ${({ theme }) => theme.colors.primary_light};
    width: ${RFPercentage(40)};
    height:${RFPercentage(5)};
    margin: ${RFValue(5)}px;
    border-radius:5px;

`  ;
