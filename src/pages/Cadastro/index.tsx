import React from 'react';
import { StyleSheet } from 'react-native'
import { Container, Elements, Logo, Input } from '../Cadastro/style';

export function Cadastro(){
    return(
        <Container source={require('../../imagens/bgimg.png')}>
            <Elements style={{top:-100}}>
                <Logo source={require('../../imagens/logo_white.png')}/>
            </Elements>
            <Elements style={{top:-50}}>
                <Input placeholder='Nome' placeholderTextColor={'#FFF'} style={styles.textInput}/>
                <Input placeholder='Sobrenome'  placeholderTextColor={'#FFF'} style={styles.textInput}/>
                <Input/>
            </Elements>
            <Elements style={{top: 0}}>
                <Input placeholder='Email' placeholderTextColor={'#FFF'} style={styles.textInput}/>
                <Input placeholder='Senha' placeholderTextColor={'#FFF'} style={styles.textInput}/>
                <Input placeholder='Confirmar Senha' placeholderTextColor={'#FFF'} style={styles.textInput}/>
            </Elements>
            <Elements style={{top:50}}>
                <Input placeholder='Enviar Foto' placeholderTextColor={'#FFF'} style={styles.textInput}/>
            </Elements>
        </Container>
    )
}

const styles = StyleSheet.create({
    textInput:{
        textAlign: 'center'
    }
})