import styled from 'styled-components/native';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';
export const Container = styled.ImageBackground`
    flex:1;
    flex-direction: column;
    position: absolute;
    justify-content: center;
    align-items: center;
    width: 100%;
    height:100%;
`; 

export const LButton = styled.TouchableOpacity`
    background: ${({ theme }) => theme.colors.button};
    width: ${RFPercentage(30)};
    height:${RFPercentage(5)};
    margin-top: ${RFValue(35)};
    border-radius:5px;

`;

export const IButton = styled.TouchableOpacity`
    position:relative;
    bottom: ${RFValue(-100)};
    background: ${({ theme }) => theme.colors.button};
    width: ${RFPercentage(20)};
    height:${RFPercentage(5)};
    border-radius:5px;
`;

export const Logo = styled.Image`
    width: ${RFPercentage(10)};
    height: ${RFPercentage(10)};
    margin-bottom: ${RFValue(90)};
`;

export const Input = styled.TextInput`
    background-color: ${({ theme }) => theme.colors.primary_light};
    width: ${RFPercentage(40)};
    height:${RFPercentage(5)};
    margin: ${RFValue(5)}px;
    border-radius:5px;

`;

export const Elements = styled.View`
    width: 100%;
    position:relative;
    top: ${RFValue(-100)};
    align-items: center;
`;


