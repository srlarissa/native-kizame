import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Input, Elements, Container,LButton, Logo, IButton } from './style'

function handlerLogin(){
    console.log('Logou!')
}

function handlerSignIn(){
    console.log('Se inscreveu!')
}

export function Login(){
    return(
        <Container source={require('../../imagens/bgimg.png')} style={{top:0}}>
            <Elements>
                <Logo source={require('../../imagens/logo_white.png')}/>
                <Input placeholder={"Email"} placeholderTextColor={'#FFFFFF'} style={styles.textInput}/>
                <Input placeholder={"Senha"} placeholderTextColor={'#FFFFFF'} style={styles.textInput}/>
                <LButton onPress={handlerLogin} activeOpacity={.7}> 
                    <Text style={styles.textButton}>Login</Text>
                </LButton>
            </Elements>
            <IButton onPress={handlerSignIn} activeOpacity={.7}>
                <Text style={styles.textButton}>Inscreva-se</Text>
            </IButton>
        </Container>    
    )
}

const styles = StyleSheet.create({
    textInput:{
        textAlign: 'center'
    },
    textButton:{
        textAlign: 'center',
        padding:10,
        color: '#403838',
        fontWeight: 'bold',
        fontSize: 16
    }
})
