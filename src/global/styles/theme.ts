export default {
    colors:{
        primary: 'rgba(140,140,140,1)',
        primary_light: 'rgba(140,140,140,0.5)',

        secondary: 'rgba(217,217,217,1)',
        secondary_light: 'rgba(217,217,217,0.3)',

        light_button:'rgba(217,217,217,0.7)',
        button:'rgba(217,217,217,1)',


    }
}