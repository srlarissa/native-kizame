
import theme from './src/global/styles/theme';
import 'react-native-gesture-handler';
import React from 'react';

import { LoadingScreen } from './src/pages/LoadingScreen';
import {Login} from './src/pages/Login';
import {Cadastro} from './src/pages/Cadastro';

import { ThemeProvider } from 'styled-components';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LoadingScreen" component={LoadingScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Cadastro" component={Cadastro} />
    </Stack.Navigator>
  );
}


export default function App() {
  return(
   <ThemeProvider theme={theme}>
      <NavigationContainer>
          <Login />
      </NavigationContainer>
   </ThemeProvider>
  ) 
}
